% Authors - Basaran, Baumberger, Dietsch, Wert

function [angle_predicted_class] = kNearestNeighbor(k,classes_vector,euc_dist_vect)
% Return the k-nearest neigbourgh class from the computed euclidean distance
%
% Arguments:
% - k, number of neighbors
% - classes_vector, the vectors of the corresponding angle classes of the training data
% - euc_dist_vect,the vector of the calculated euclidean distances
%
% Return Value:
% - angle_predicted_class, the class of the angle predicted

k_nn_angle_est = zeros(1,k);    

% Sort the euclidean distances in order to have the min distances at the
% beggining
euc_dist_vect_sorted = sort(euc_dist_vect);

% Observe the k nearest neighbors
    for i_k = 1:k
        nn_idx = euc_dist_vect == euc_dist_vect_sorted(i_k);
        nn_angle_idx = find(nn_idx.*classes_vector);
        k_nn_angle_est(i_k) = classes_vector(nn_angle_idx(1));
    end
    [GC,GR] = groupcounts(k_nn_angle_est');
    [~,idx] = max(GC);
    angle_predicted_class = GR(idx);
end
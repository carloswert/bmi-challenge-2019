# No To Animal Experimentation: K-Nearest Neighbours Approach for Primate Arm Movement Regression

No To Animal Experimentation is a MATLAB software for arm movement regression using a K-NN approach.

## Installation

Unzip the folder and run the code "testFunction_for_students_MTb.m" inside the NoToAnimalExperimentation.

The folder LstImplementation contains further codes for the neural network approach that was unused in the end and the scripts used for 
the cross-validation and model comparison.


## Usage

Open the MATLAB script "testFunction_for_students_MTb.m" and the software will automatically train and test itself with the provided data.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
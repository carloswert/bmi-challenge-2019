function gradients=lstm_backward(da,cache)
    rems=cache.rems;
    x=cache.x(:,:,1);
    a1=rems(1).a_next;
    c1=rems(1).c_next;
    a0=rems(1).a_prev;
    c0=rems(1).c_prev;
    f1=rems(1).ft;
    i1=rems(1).it;
    cc1=rems(1).cct;
    o1=rems(1).ot;
    x1=rems(1).xt;
    param=rems(1).param;
    
    [n_a, m, T_x]=size(da);
    [n_x, m]=size(x);
    
    dx=zeros(n_x, m, T_x);
    da0=da;
    da_prevt=zeros(n_a,m);
    dc_prevt=zeros(n_a, m);
    dWf=zeros(n_a, n_a + n_x);
    dWi=zeros(n_a, n_a + n_x);
    dWc=zeros(n_a, n_a + n_x);
    dWo=zeros(n_a, n_a + n_x);
    dbf=zeros(n_a, 1);
    dbi=zeros(n_a, 1);
    dbc=zeros(n_a, 1);
    dbo=zeros(n_a, 1);
    
    for t =(T_x:-1:1)
        grads=lstm_cell_backward(da(:,:,t), dc_prevt, cache.rems(t));
        dx(:,:,t)=grads.dxt;
        dWf=dWf+grads.dWf;
        dWi=dWi+grads.dWi;
        dWc=dWc+grads.dWc;
        dWo=dWo+grads.dWo;
        dbf=dbf+grads.dbf;
        dbi=dbi+grads.dbi;
        dbc=dbc+grads.dbc;
        dbo=dbo+grads.dbo;
    end

    da0=da_prevt;
    gradients.dx=dx;
    gradients.da0=da0;
    gradients.dWf=dWf;
    gradients.dbf=dbf;
    gradients.dWi=dWi;
    gradients.dbi=dbi;
    gradients.dWc=dWc;
    gradients.dbc=dbc;
    gradients.dWo=dWo;
    gradients.dbo=dbo;
    
end

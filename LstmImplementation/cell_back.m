function [gradient]=cell_back(da_n, rem)
%{
Implements the backward pass for the RNN-cell (single time-step).

Input
da_next: Loss gradient of next hidden
rem:Feed-forward values

Ouput:
dx: Gradient of input (n_x, m)
da_prev:Gradients of previous hidden state, of shape (n_a, m)
dWax:Gradients of input-to-hidden weights, of shape (n_a, n_x)
dWaa:Gradients of hidden-to-hidden weights, of shape (n_a, n_a)
dba:Gradients of bias vector, of shape (n_a, 1)
%}
a_n=rem.a_n;
a_p=rem.a_p;
x=rem.x;
param=rem.param;

Wax=param.Wax;
Waa=param.Waa;
Wya=param.Wya;
ba=param.ba;
by=param.by;


% compute the gradient of the loss with respect to Wax (?2 lines)
dtanh=(1-a_n.*a_n).*da_n;
dx=Wax'*dtanh;
dWax=dtanh*x';

% compute the gradient with respect to Waa (?2 lines)
da_p=Waa'*dtanh;
dWaa=dtanh*a_p';

%compute the gradient with respect to b (?1 line)
dba=sum(dtanh,2);
gradient.dxt=dx;
gradient.da_p=da_p;
gradient.dWax=dWax;
gradient.dWaa=dWaa;
gradient.dba=dba;
end
function grads=lstm_cell_backward(da_next, dc_next, cache)
    a_next=cache.a_next;
    c_next=cache.c_next;
    a_prev=cache.a_prev;
    c_prev=cache.c_prev;
    ft=cache.ft;
    it=cache.it;
    cct=cache.cct;
    ot=cache.ot;
    xt=cache.xt;
    param=cache.param;
    
    [n_x, m]=size(xt); 
    [n_a, m]=size(a_next); 
    
    dott=da_next.*tanh(c_next).*ot.*(1-ot);
    dcct=(dc_next.*it+ot.*(1-(tanh(c_next)).^2).*it.*da_next).*(1-(cct).^2);
    dit=(dc_next.*cct+ot.*(1-(tanh(c_next)).^2).*cct.*da_next).*it.*(1-it);
    dft=(dc_next.*c_prev+ot.*(1-(tanh(c_next)).^2).*c_prev.*da_next).*ft.*(1-ft);

    dWf=dft*[a_prev' xt'];
    dWi=dit*[a_prev' xt'];
    dWc=dcct*[a_prev' xt'];
    dWo=dott*[a_prev' xt'];
    dbf=sum(dft,2);
    dbi=sum(dit,2);
    dbc=sum(dcct,2);
    dbo=sum(dott,2);

 
    da_prev=param.Wf(:,1:n_a)'*dft+param.Wi(:,1:n_a)'*dit+param.Wc(:,1:n_a)'*dcct+param.Wo(:,1:n_a)'*dott;
    dc_prev=dc_next.*ft+ot.*(1-(tanh(c_next)).^2).*ft.*da_next;
    dxt=param.Wf(:,n_a+1:end)'*dft+param.Wi(:,n_a+1:end)'*dit+param.Wc(:,n_a+1:end)'*dcct+param.Wo(:,n_a+1:end)'*dott;
    
    grads.dxt=dxt;
    grads.da_prev=da_prev;
    grads.dc_prev=dc_prev;
    grads.dWf=dWf;
    grads.dbf=dbf;
    grads.dWi=dWi;
    grads.dbi=dbi;
    grads.dWc=dWc;
    grads.dbc=dbc;
    grads.dWo=dWo;
    grads.dbo=dbo;
end
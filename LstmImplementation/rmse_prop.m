function [param,grad_square]=rmse_prop(lr,param,gradient,grad_square,clip)
names = fieldnames(param);
dnames = fieldnames(gradient);
ep=1e-9;
for i = 1:length(names)
w=param.(names{i});
dw=gradient.(join(['d',names{i}]));
dw2=grad_square.(join(['d',names{i}]));
dw2 = 0.9*dw2 + 0.1*dw.*dw;
%Clipping gradients
%dw(dw>clip)=clip;
%dw(dw<-clip)=-clip;
w = w-(lr*dw./sqrt(dw2+ep));
param.(names{i})=w;
grad_square.(join(['d',names{i}]))=dw2;
end
end
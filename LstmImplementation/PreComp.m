clc
clear all
close all
load ("monkeydata_training.mat")
fs=1e-3;
%% Raster plot
%We will use the first trial
figure(1)
for nang=1:size(trial,2)
    subplot(3,3,nang)
    [i,j,v] = find(trial(1,nang).spikes);
    scatter(fs*j,i);
end

%% One Neuron Many Trials
neuron = randsample(size(trial(1,1).spikes,1),1);
movement = randsample(size(trial,2),1);
neuron = 1;
movement = 1;
figure(2)
v = [];
for i=1:size(trial,1)
    one_neuron{i}=find(trial(i,movement).spikes(neuron,:))*fs;
    hold on
    v = [v fs*find(trial(i,movement).spikes(neuron,:))];
    scatter(one_neuron{i},i*ones(1,length(one_neuron{i})));
end
xlabel('Time(s)')
ylabel('Trial')
%% PSTH
figure(3)
histogram(v,100)
xlabel('Time(s)')
ylabel('Trial')

%% Positions
figure(4)
for n=1:10
plot3(trial(n,movement).handPos(1,:),trial(n,movement).handPos(2,:),trial(n,movement).handPos(3,:));
hold on
end
%% Tunning curves

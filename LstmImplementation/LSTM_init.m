function [param,a0] = LSTM_init(n_a,n_x,n_y,m)
    
param.Wf=rand(n_a, n_a + n_x);
param.Wi=rand(n_a, n_a + n_x);
param.Wc=rand(n_a, n_a + n_x);
param.Wo=rand(n_a, n_a + n_x);
param.bf=rand(n_a, 1);
param.bi=rand(n_a, 1);
param.bc=rand(n_a, 1);
param.Wy=rand(n_y, n_a);
param.by=rand(n_y,1);
param.bo=rand(n_a, 1);
a0=zeros(n_a, m);
end
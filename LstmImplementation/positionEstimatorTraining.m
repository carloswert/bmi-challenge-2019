function parameters = positionEstimatorTraining(trainingData)
time_max=561; %Normalization of sample size for time
time_samples=560; %Number of maximum time frames
train=[];
%Combine elements
for i=(1:size(trainingData,2))
    for j=(1:size(trainingData,1))
        train=[train trainingData(j,i)];
    end
end
n_units=7; %Number of RNN units
ny=2; %Define number of output variables
nx=size(train(1).spikes,1);
clipping=1; %Maximum values of dParam.
%Initialization of parameters
lr=0.5;
epochs=20;
batch_size=200;
[param,a0] = LSTM_init(n_units,nx,ny,batch_size);%Initizalize weights
%Data preparation and normalization for ReLU
for nn=(1:length(train))
    x_train(:,nn,:)=train(nn).spikes(:,1:time_max);
    y_train(1,nn,:)=train(nn).handPos(1,1:time_max);
    y_train(2,nn,:)=train(nn).handPos(2,1:time_max);
end
for iter=(1:epochs)
   for batch=[1:batch_size:size(x_train,2)]
       if batch+batch_size>size(x_train,2)
           continue
       else
       for t=(1:20:time_max-time_samples)
       x_batch=x_train(:,batch:batch+batch_size-1,t:t+time_samples);
       y_batch=y_train(:,batch:batch+batch_size-1,t:t+time_samples);
       [a, y_p, c, caches]=lstm_forward(x_batch, a0, param);
       [loss, da,dWy,dby]=loss_grad(y_batch,y_p,a,param,clipping);
       gradients=lstm_backward(da,caches);
       gradients.dWy=dWy;
       gradients.dby=dby;
           if ~exist('grad_square', 'var') || isempty(grad_square)
                grad_square = gradients;
           end;
       [param,grad_square]=rmse_prop(lr,param,gradients,grad_square,clipping);
       end
       end
        fprintf('=')
   end;
    fprintf('Epoch: %d  ',iter)
    fprintf('Loss: %f\n',loss)
end
parameters=param;
end
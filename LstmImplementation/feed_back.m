function gradient =  feed_back(da, cache)
%{
Implement the backward pass for a RNN over an entire sequence of input data.

Arguments:
da -- Upstream gradients of all hidden states, of shape (n_a, m, T_x)
caches -- tuple containing information from the forward pass (rnn_forward)

Returns:
gradients -- python dictionary containing:
    dx -- Gradient w.r.t. the input data, numpy-array of shape (n_x, m, T_x)
    da0 -- Gradient w.r.t the initial hidden state, numpy-array of shape (n_a, m)
    dWax -- Gradient w.r.t the input's weight matrix, numpy-array of shape (n_a, n_x)
    dWaa -- Gradient w.r.t the hidden state's weight matrix, numpy-arrayof shape (n_a, n_a)
    dba -- Gradient w.r.t the bias, of shape (n_a, 1)
%}


%Initial cache data
rem0 = cache.rem(1);
x=cache.x(:,:,1);
a1=rem0.a_n;
a0=rem0.a_p;
param=rem0.param;
x1=rem0.a_n;

%Retrieve dimensions from da's and x1's shapes (?2 lines)
[na,m,Tx]=size(da);
nx=size(x,1);

% initialize the gradients with the right sizes (?6 lines)
dx=zeros(nx,m,Tx);
dWax=zeros(na,nx);
dWaa=zeros(na,na);
dba=zeros(na,1);
da0 = zeros(na,m);
da_p = zeros(na,m);

% Loop through all the time steps
for t=(Tx-1:-1:1)
    % Compute gradients at time step t.
    % Remember to sum gradients from the output path (da) and the previous timesteps (da_prevt) (?1 line)
    gradient=cell_back((da(:,:,t)+da_p),cache.rem(t));
    % Retrieve derivatives from gradients (? 1 line)
    dxt=gradient.dxt;
    da_p=gradient.da_p;
    dWax_l=gradient.dWax;
    dWaa_l=gradient.dWaa;
    dba_l=gradient.dba;
    % Increment global derivatives w.r.t parameters by adding their derivative at time-step t (?4 lines)
    dx(:, :, t)=dxt;
    dWax=dWax+dWax_l;
    dWaa=dWaa+dWaa_l;
    dba=dba+dba_l;
end;
% Set da0 to the gradient of a which has been backpropagated through all time-steps (?1 line) 
da0=da_p;

gradient.dx=dx;
gradient.da0=da0;
gradient.dWax=dWax;
gradient.dWaa=dWaa;
gradient.dba=dba;

end
function [a_next, c_next, yt_pred, cache]= lstm_cell_forward(xt, a_prev, c_prev, param)
    Wf=param.Wf;
    bf=param.bf;
    Wi=param.Wi;
    bi=param.bi;
    Wc=param.Wc;
    bc=param.bc;
    Wo=param.Wo;
    bo=param.bo;
    Wy=param.Wy;
    by=param.by;

    [n_x, m]=size(xt);
    [n_y, n_a]=size(Wy);
    concat=[a_prev;xt];
    sigmoid=@(x) 1./(1+exp(-x));
    ft=sigmoid(Wf*concat+bf);
    it=sigmoid(Wi*concat+bi);
    cct=tanh(Wc*concat+bc);
    c_next=ft.*c_prev+it.*cct;
    ot=sigmoid(Wo*concat+bo);
    a_next=ot.*tanh(c_next);
    yt_pred=max(Wy*a_next+by,zeros(size(by)));
    
    cache.a_next=a_next;
    cache.c_next=c_next;
    cache.a_prev=a_prev;
    cache.c_prev=c_prev;
    cache.ft=ft;
    cache.it=it;
    cache.cct=cct;
    cache.ot=ot;
    cache.xt=xt;
    cache.param=param;

end
function [decodedPosX, decodedPosY]=positionEstimator(past_current_trial, modelParameters)
x_batch=past_current_trial.spikes;
total_time=size(x_batch,2);
decodedPosX=[];
decodedPosY=[];
time_samples=19;
n_a=length(modelParameters.bo);
for t=(1:time_samples:total_time-time_samples)
x(:,1,:) = x_batch(:,t:t+time_samples);
[a, y_p, c, caches]=lstm_forward(x, zeros(n_a,1), modelParameters);
y_p=squeeze(y_p);
decodedPosX=[decodedPosX y_p(1,:)];
decodedPosY=[decodedPosY y_p(2,:)];
end
end
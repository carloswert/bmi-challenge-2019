function [param,a0] = RNN_init(na,nx,ny,m)
%{
nx: Features of x
m: Samples
Tx: Times
Input
x:Input (nx, m, Tx).
a0:Initial hidden (na, m)
Waa:Weight hidden state (na, na)
Wax:Weight input (na, nx)
Wya:Weight hidden-state to output(ny, na)
ba:Bias (na, 1)
by:Bias (ny, 1)
%}
param.Wax=rand(na,nx);
param.Waa=rand(na,na);
param.Wya=rand(ny,na);
param.ba=rand(na,1);
param.by=rand(ny,1);
a0=zeros(na, m);
end
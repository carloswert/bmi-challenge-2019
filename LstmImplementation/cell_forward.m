function [a_n, y_pt, rem] = cell_forward(xt, a_p, param)
%{ 
Feedforward RNN with tanh activation function

Input
x: Input at time step with (nx, m)
a_p: Previous hidden state
Wax: Weight of input (na,nx)
Waa: Weight of hidden state (na,na)
Wya: Weight of hidden to output (ny,na)
ba: Bias (na,1)
by: Output bias (ny,1)

Output
a_n:Next layer (na,m)
y: Prediction at timestep (ny,m)
back: Parameters for backward prop (a_next, a_prev, xt, parameters).
 %} 
Wax = param.Wax;
Waa = param.Waa;
Wya = param.Wya;
ba = param.ba;
by = param.by;
a_n = tanh(Waa*a_p+Wax*xt+ba);
n_y=size(Wya,1);
m=size(a_n,2);
y_pt = max(Wya*a_n+by,zeros(n_y,m));

rem.a_n=a_n;
rem.a_p=a_p;
rem.x=xt;
rem.param=param;
end
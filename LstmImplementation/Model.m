%Model
clc
clear all
close all
load('monkeydata_training.mat');
time_max=593; %Normalization of sample size for time
time_samples=20; %Number of maximum time frames
t_disp=293;
%% Mix all data
%Combine elements
train=[];
for i=(1:1)
    for j=(1:size(trial,1))
        train=[train trial(j,i)];
        plot(trial(j,i).handPos(1,1:560),trial(j,i).handPos(2,1:560))
        hold on
    end
end
%% Model Creation
n_units=800; %Number of RNN units
ny=2; %Define number of output variables
nx=size(train(1).spikes,1);
clipping=1; %Maximum values of dParam.
%Initialization of parameters
lr=0.1;
epochs=15;
%% K-fold Cross-Validation
indexes = randperm(size(trial,2)*size(trial,1)); %Shuffle elements
batch_size=80;
%[param,a0] = RNN_init(n_units,nx,ny,batch_size);%Initizalize weights
[param,a0] = LSTM_init(n_units,nx,ny,batch_size);%Initizalize weights
%Data preparation and normalization for ReLU
for nn=(1:length(train))
    x_train(:,nn,:)=train(nn).spikes(:,1:time_max);
    y_train(1,nn,:)=train(nn).handPos(1,1:time_max);
    y_train(2,nn,:)=train(nn).handPos(2,1:time_max);
    
end
for iter=(1:epochs)
   for batch=[1:batch_size:size(x_train,2)]
       if batch+batch_size>size(x_train,2)
           continue
       else
       for t=(1:time_samples:time_max-time_samples)
       x_batch=x_train(:,batch:batch+batch_size-1,t:t+time_samples);
       y_batch=y_train(:,batch:batch+batch_size-1,t_disp:t_disp+time_samples);
       %[a, y_p, cache]=feed_forward(x_batch,y_batch, a0, param);
       [a, y_p, c, caches]=lstm_forward(x_batch, a0, param);
       [loss, da,dWy,dby]=loss_grad(y_batch,y_p,a,param);
       %gradient=feed_back(da, cache);
       gradients=lstm_backward(da,caches);
       gradients.dWy=dWy;
       gradients.dby=dby;
       %Create grad_square if it doesn't exist
       if ~exist('grad_square', 'var') || isempty(grad_square)
            grad_square = gradients;
       end;
       [param,grad_square]=rmse_prop(lr,param,gradients,grad_square,clipping);
       end
       end
        fprintf('=')
   end;
    fprintf('Epoch: %d  ',iter)
    fprintf('Loss: %f\n',loss)
end
%%
n_a=length(param.bo);
for i=(1:20)
    pause(0.001);
    hold on
    for t=(1:time_samples:time_max-time_samples)
    x(:,1,:) = train(i).spikes(:,t:t+time_samples);
    [a, y_p, c, caches]=lstm_forward(x(:,1,:) , zeros(n_a,1), param);
    plot(y_p(1,:),y_p(2,:),'b')
    end
    plot(train(i).handPos(1,:),train(i).handPos(2,:), 'r');
end
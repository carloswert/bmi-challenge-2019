function [loss,da,dWy,dby] = loss_grad(y_train,y_p,a,param,clip)

loss=mean(rms(squeeze(vecnorm(y_train-y_p,2,1).^2),1));
Tx=size(y_p,3);

ny=size(y_p,1);
da=zeros(size(a));
Wy=param.Wy;
dWy=zeros(size(Wy));
dby=zeros(size(param.by));
%Wya: Weight of hidden to output (ny,na)

for t=(1:size(y_p,3))
   da(:,:,t)=Wy'*2*(y_p(:,:,t)-y_train(:,:,t))/ny; %(ny,m,Tx)
   dWy=dWy+2*(y_p(:,:,t)-y_train(:,:,t))*a(:,:,t)'/ny;
   dby=dby+2*sum(y_p(:,:,t)-y_train(:,:,t),2)/ny;
end;
%da(da>clip)=clip;
%da(da<clip)=-clip;
end
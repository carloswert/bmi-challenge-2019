function [a, y, c, caches]=lstm_forward(x, a0, param)
    [n_x, m, T_x]=size(x);
    [n_y, n_a]=size(param.Wy);
    a=zeros(n_a, m, T_x);
    c=zeros(n_a, m, T_x);
    y=zeros(n_y, m, T_x);

    a_next=a0;
    c_next=zeros(n_a,m);
    rems=[];
    for t=(1:T_x)
        [a_next, c_next, yt, rm]=lstm_cell_forward(x(:,:,t), a_next, c_next, param);
        a(:,:,t)=a_next;
        y(:,:,t)=yt;
        c(:,:,t)=c_next;
        rems=[rems rm];
    end
    
    caches.rems=rems;
    caches.x=x;
end
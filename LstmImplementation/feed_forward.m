function [a, y_p, cache] = feed_forward(x,y, a0, param)
%{
Input
x:Input (nx, m, Tx).
a0:Initial hidden (na, m)
Waa:Weight hidden state (na, na)
Wax:Weight input (na, nx)
Wya:Weight hidden-state to output(ny, na)
ba:Bias (na, 1)
by:Bias (ny, 1)

Output
a:All hidden states (na, m, Tx)
yp:Predictions at every time step (ny, m, Tx)
rem:Backward pass (param, x)
%}

rem=[]; %initialize list of rem

%Initialization parameters
[nx,m,Tx]=size(x);
[ny, na]=size(param.Wya);

a=zeros(na,m,Tx);
y_p=zeros(ny,m,Tx);
a_n=a0;

%Loop over time
for t=(1:Tx)
    [a_n, y_pt, rm]=cell_forward(x(:,:,t), a(:,:,t), param); %Obtain the prediction cell
    a(:,:,t+1)=a_n; %Get the next hidden parameter
    y_p(:,:,t)=y_pt; %Save current prediction
    rem = [rem rm]; %Append reminder information
end

cache.rem=rem;
cache.x=x;
end